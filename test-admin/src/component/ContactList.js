import { TextField, Datagrid, List} from 'react-admin'
import * as React from 'react'
export const ContactList = props => {
    return (
        <List {...props}>
            <Datagrid>
                <TextField source='id' />
                <TextField source='name' />
                <TextField source='number' />
            </Datagrid>
        </List> 
    )
}