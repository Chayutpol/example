import * as React from "react";
import { Admin, ListGuesser, Resource } from 'react-admin';
import postgrestRestProvider from '@raphiniert/ra-data-postgrest';
import { ContactList } from "./component/ContactList";

const dataProvider = postgrestRestProvider('http://localhost:5000');
const App = () => (<Admin dataProvider={dataProvider} >
  <Resource name="contact" list={ContactList} />
</Admin>)

export default App;